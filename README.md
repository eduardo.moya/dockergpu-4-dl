# dockergpu-4-dl

Create docker file with pytorch for an *amd* gpu Card

## Getting started

Inside of the `docker-amd`  folder run
```bash
docker compose -f  docker-compose.yml build
```
The base image is the [rocm/pytorch](https://hub.docker.com/r/rocm/pytorch/#!)

## Max Docker Size

Please be aware that the image docker size  could be more than 10 GB. Could be necesary increase the maximum size of images. Check this [link](https://guide.blazemeter.com/hc/en-us/articles/115003812129-Overcoming-Container-Storage-Limitation)  


## License
To do
